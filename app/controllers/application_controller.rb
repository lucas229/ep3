
class ApplicationController < ActionController::Base


    rescue_from CanCan::AccessDenied do
        flash[:notice] = "Logue como administrador para realizar esta ação!"
        redirect_to root_path
    end


    private
        def current_cart
            Cart.find(session[:cart_id])
        rescue ActiveRecord::RecordNotFound
            cart = Cart.create
            session[:cart_id] = cart.id
            cart
        end

        helper_method :current_cart

end
