class Category < ApplicationRecord
    has_many :games
    validates :name, presence: { message: 'não pode ficar em branco'} , uniqueness: { message: 'já criado'}
end
