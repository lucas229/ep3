class LineItem < ApplicationRecord
    belongs_to :game
    belongs_to :cart
    def total_price
        game.price * quantity
    end
end
