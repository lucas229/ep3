class Game < ApplicationRecord
    mount_uploader :picture, PictureUploader
    belongs_to :category
    validates :name, presence: { message: 'não pode ser deixado em branco' }, uniqueness: { message: 'já criado'} 
    validates :price, presence: { message: 'não pode ser deixado em branco'}, numericality: {message: ':valor não aceito'}
    validates_presence_of :picture
    has_many :line_items
    before_destroy :ensure_not_referenced_by_any_line_item
    private
        def ensure_not_referenced_by_any_line_item
            if line_items.empty?
                return true
            else
                errors.add(:base, 'Line Items present')
                return false
            end
        end
end
