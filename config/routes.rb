Rails.application.routes.draw do

  get 'stores/index'
  devise_for :users
  resources :line_items
  resources :carts
  root 'stores#index'
  resources :categories
  resources :games
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
