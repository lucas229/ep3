class CombineItemsInCart < ActiveRecord::Migration[6.0]

  def up
    Cart.all.each do |cart|
      sums = cart.line_items.group(:game_id).sum(:quantity)
      sums.each do |game_id, quantity|
        if quantity > 1
          cart.line_items.where(:game_id=>game_id).delete_all
          cart.line_items.create(:game_id=>game_id, :quantity=>quantity)
        end
      end
    end
  end

  def down
    LineItem.where("quantity>1").each do |line_item| 
      line_item.quantity.times do
        LineItem.create :cart_id=>line_item.cart_id, :game_id=>line_item.game_id, :quantity=>1 
      end
      line_item.destroy
    end
  end


end
