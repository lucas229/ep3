# EP3
    
    Lucas Rodrigues Fonseca - 18/0114077
    Kleidson Alves Corrêa - 18/0113861

## Instruções
* git clone https://gitlab.com/lucas229/ep3.git
* cd ep3
* bundle install
* yarn install --check-files
* rails db:migrate
* Para criar um administrador execute o comando rails db:seed (Email: admin@email.com, Senha: admin123)
* rails server

## Descrição do projeto
A proposta de criação: uma loja virtual de jogos. O usuário tem acesso ao catálogo de jogos e poderá adicionar ao carrinho os jogos que desejar na quantidade que desejar comprar. Porém, só poderá realizar a compra quando estiver registrado e logado no sistema. Há um outro tipo de usuário: um administrador. Ele poderá fazer as mesmas coisas que um usuário normal e ainda utilizar outros recursos do sistema. Um administrador pode cadastrar uma nova categoria e adicionar/remover jogos do catálogo. 

## Diagramas 

1. Diagramas de Classe :

    Controllers: 
    <img src="diagramas/Controllers.png">
    
    
    Models: 
    <img src="diagramas/Models.png">
    
2. Caso de uso: 
    
    <img src="diagramas/caso_de_uso.png">

# ep3

Projeto 3 da disciplina Orientação a Objetos

# Requisitos Mínimos do EP3
  
1. Projeto de Aplicativo Web em Ruby on Rails

1. Incluir em sua modelagem pelo menos duas classes de dados (model) além dos usuários.

1. A aplicação deverá gerenciar o cadastro de usuários com acesso à funções exclusivas, além das funcionalidades públicas da aplicação.

1. Interface com layout elaborado para além do padrão do Rails (Usando CSS, Bootstrap, Matrerial design, etc.)

1. Descrição da solução completa no arquivo Readme.md do repositório contendo: descrição da solução com os objetivos e funcionalidades, o diagrama de classes tanto das Models quanto das Controllers, o diagrama de casos de uso da aplicação.

 
# Requisitos para maior pontuação no trabalho

1. Criar relação entre as models

1. Gerência de níveis de permissão por tipo de usuário (usuário, regular, usuário admin, etc)

1. Uso de outras gems, além das que já são padrão no projeto inicial do Rails.

1. Relevância do tema, criatividade e refinamento na implementação.


# Entrega do EP3 consistirá de:

1. Link para o repositório GIT com o código fonte da aplicação.